 How to setup the server for the Config/Eureka Servers
========================================================

create two instances in vpc private subnet

create security group with these open <br/>
TCP PORT 8888 <br/>
TCP PORT 8889 <br/>
TCP PORT 8761 <br/>
TCP PORT 8762 <br/>
SSH PORT <br/>

Create two Route 53 urls that point to the two servers. <br/>
discovery.mthinx.com <br/>
discovery-peer.mthinx.com <br/>


install java 8 - script at root of projects - run as sudo.

fix the /etc/hosts file

create /home/ubuntu/configserver directory for configserver <br/>
add the setenv-prod.sh from the config server project to this directory <br/>
copy the ‘container’ jar, from configserver-dist/ to this directory. <br/>
add the configserver file from the config server project /etc/init.d <br/>
make sure files are executable. <br/> <br/>

Install service — <br/>
sudo chmod +x /etc/init.d/configserver <br/>
sudo chown root:root /etc/init.d/configserver <br/>
sudo update-rc.d configserver defaults <br/>
sudo update-rc.d configserver enable <br/>

You need to make a change to the setenv-prod.sh file.  We need to set the HOST exported variable.  It needs to be
the Route 53 DNS name given to the server that this service is running on.  This is important b/c clients will use this
address to connect to the configuration server when using eureka service discovery.

start service - sudo service configserver start



create /home/ubuntu/micloudeureka directory for eurekaserver <br/>
add the setenv-prod.sh from the eureka server project to this directory <br/>
copy the ‘container’ jar, from micloudeureka-dist/, to this directory. <br/>
add the micloudeureka file from the eureka server project /etc/init.d <br/>
make sure files are executable. <br/>

Install service — <br/>
sudo chmod +x /etc/init.d/micloudeureka <br/>
sudo chown root:root /etc/init.d/micloudeureka <br/>
sudo update-rc.d micloudeureka defaults <br/>
sudo update-rc.d micloudeureka enable <br/>

 use setenv-prod.sh for the eureka peer one <br/>
 use setenv-prod-peer.sh for the eureka peer two (rename to just setenv-prod.sh) <br/>

start service - sudo service micloudeureka start