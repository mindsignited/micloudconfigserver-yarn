package com.mindsignited.container;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.OpenSshConfig.Host;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.util.FS;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigServer
public class ContainerApplication {

    private static final String PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "Proc-Type: 4,ENCRYPTED\n" +
            "DEK-Info: AES-128-CBC,CB6D98984890F8866E749DE912FFB245\n" +
            "\n" +
            "HfFDAWPHW8BCjoJSNIrt4Mz5pt9nl3OS4Pw2vP8Ro9rQtZPha1Q7m1d4+YkNKk3i\n" +
            "vnLr+YARLDLeow9u25VCYkMJrWiWL/jjR4hDCeKf5CSIYRGKBpmG61aLT248GNtN\n" +
            "ohitShxZASot/JQC1nATkrzrSwgrZHgctEB/BZgM9v0dEV7ilMn1iLrV1RoTwnei\n" +
            "zMbJgx5Se6IU/oJxXaamSG/INdWpoqX7vCvCtIl9/n6WGyAKHzAw1YTGqXFgzuXd\n" +
            "y6FD5/o39YmSJxmpkMFYH2IN9tIOueNcxn5mOo22rs25xDBwcLXLP1I49dU95l9P\n" +
            "IL8j26qvhYFrm8AYigMSf0GPTDQQIvlZ1b1UG7afzgLLCKXS/aTKl7FetuFjSFt4\n" +
            "+2dCTX5t6qPwc9pOO8F9fmqySf6sSSBMa2LwS+tDsTJqabbQYC/GY1Y/fh2dI1pU\n" +
            "/nrbgfPuzjETWrHHNI2xtPU4gqmQ16p2e9Ga+U+aP4nixmWXo1a4+hwvWQl/qbb7\n" +
            "mqE7KVgCACD3HFfTxSMXllxcWXEsucthG1Bb6JsMyo5OQ0V737irrwy5XT1MQ7R3\n" +
            "r/9YofGsNZTLFxjTrI83hE3XL7LMDQ2hzbZ/BSWgsh9NHgJtZb0LlXh8MFlOhEqG\n" +
            "SAVqLX4869YXYBSouijt/46gu34lVT3YD5aJMM/tA9+mDIn0TtXMBCAjjNIaX+BA\n" +
            "WFO+K1kZcjSQWDJGv/ploqsUniV5SJnH8TSUDAyWz/AQ7+K58N4Vl93t/r2Gy9vX\n" +
            "FoH7DND0XDRkybBAi4RJuHysCF2QDAF8vg6NqFa6OSodKyfitR2XzeWNpGpBj29H\n" +
            "2h4TZAqwi6Ra96EhWu3wHJaMD5tmVs9/Y1el7EDk7ap8QiuheWMNuP1Vlfxjew0V\n" +
            "iiA5qTNz6rre0pviTe7v8v11+oyzMLkwRH1E4L1rXBANRPI5yO1uj70kHOGlc6Gr\n" +
            "GDGMEzoU8+boT/PER5aBtBFWhiouvOWuO3rgBz/4rHhCQeKOLDdqux2arjjnLP7y\n" +
            "dcqflk92xUX15XvR5X2rpSWHfE5IOhGBcGm2OwSrkjxisSRJr6m2mk2yEpxzqeHH\n" +
            "8FYJsSB3LXs1u2zMU4tCMxAKaeCv7pRyseDS0S3pfUkNUTuv9qZrHcpytLNn5J0W\n" +
            "IDHxkITdR+LhAn2ggB8Ilo3xaKCKthw1K/ZHzlk9k0itmapqtdBvtalZIvgyGDm6\n" +
            "UJMVa61qkomTtHTiReiZE0gW+Yqdd/MSfbsocIX+aHE9p+ntVa/8SVhHxp/E02Pj\n" +
            "b1iy8sWcz86XRwhPEby84kB9A60tkufFzURJ0v1PfHr2RO6KTt/ifflVrpMX7HtR\n" +
            "gmptDCH4zJdR2MaAtUYl6ouUKYsCIwBkrMHbTnxz8JQHt3JgdVfUKFsGt/0591io\n" +
            "DWpb6YijIcGBRl2Ilneh7tBAGvLR/spLEz9GwnSUg+DsDeJ4dOHdmeCMhWSvH1VY\n" +
            "ZjIhCJtsGmEFCny1pDX1w5xjXUU6mlmZ91WT8/Hw0dLpogcubwCNTE0N0tAJJCTk\n" +
            "Rf0rTdIT1xAKPiCfYbqYxizoZRU5d3XhBuxYQXWTj+BBhJRJASoqWGyCXFYVh4Gk\n" +
            "-----END RSA PRIVATE KEY-----\n";
    private static final String PUBLIC_KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9FAyC9TqEt8zIIhkVGbyJL7XtPXSgRNsRGdzJXEy69Yo7ZK47ry0wX+WUGVO6/lYi9o4O3GtlM5+4QIISGRwQbTutCxcsOK0ZyZfFen1TxVXdVm22pVnti3alC9zjCat6cZnxlc/Wy/hILMLNzMjOzIzhBZ1qTftDf8dwnOzkyGPO81IJG2Od5V8SpwEF1xseHhIz3y+r37smiJB+E9PEcU1C2EexhAptDgoj2z/uy0o2lpyNQbANKFRwa5TTA4NInkEutBDCKGqQxNfEQMAWGGiYK7w4G9q3f06OlGR+h251j9jr2dFAdzljIqEq7cDFUgCAK56ZTX3tND8C5RXb nicholaspadilla@nicksmac.local\n";
    private static final String PASSPHRASE = "0n3T1m3P@sS";


    public static void main(String[] args) {
        SshSessionFactory factory = new JschConfigSessionFactory() {
            public void configure(Host hc, Session session) {
                session.setConfig("StrictHostKeyChecking", "no");
            }

            @Override
            protected JSch getJSch(final OpenSshConfig.Host hc, FS fs) throws JSchException {
                JSch jsch = super.getJSch(hc, fs);
                jsch.removeAllIdentity();
//                String content = new String(Files.readAllBytes(publicKey.getFile().toPath()));
                //Where getSshKey returns content of the private key file
                if (StringUtils.isNotEmpty(PUBLIC_KEY)) {
                    jsch.addIdentity("identityName", PRIVATE_KEY.getBytes(), PUBLIC_KEY.getBytes(), PASSPHRASE.getBytes());
                }
                return jsch;
            }
        };
        SshSessionFactory.setInstance(factory);
        SpringApplication.run(ContainerApplication.class, args);
    }

}
