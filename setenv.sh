#!/bin/bash

## app args
export APP_MEMORY=512MB
export SERVICE_PORT=8888
export MGMT_SERVICE_PORT=8889
export HOST=127.0.0.1
export HADOOP_HOSTNAME=localhost
export HADOOP_HOST_PORT=8020

## launcher args
export CONFIG_APPLICATION=configserver
export CONFIG_APPLICATION_EXTENSION=jar
export PATH_TO_EXECUTABLE=${CONFIG_APPLICATION}-dist # <-- no trailing slash here.

# adding this here b/c we'll just use the directory the executable is in
export DIRECTORY_FOR_GIT_REPO=${PATH_TO_EXECUTABLE}/gitConfiguration

# jvm arguments added here
export JVM_OPTS="" #"-Dspring.profiles.active=production"