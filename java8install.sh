#!/bin/bash

# install oracle jdk8 - java8install.sh
add-apt-repository -y ppa:webupd8team/java > /dev/null # do this without so much noise - errors will show
apt-get update -y > /dev/null # do this without so much noise - errors will show
echo debconf shard/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
apt-get install -y oracle-java8-installer > /dev/null # do this without so much noise - errors will show
apt-get install -y oracle-java8-set-default > /dev/null # do this without so much noise - errors will show