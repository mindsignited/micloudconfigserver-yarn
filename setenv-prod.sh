#!/bin/bash

##
## Environment settings - they are all defaults and not needed but are available.
##


export APP_MEMORY=1g
export SERVICE_PORT=8888
export MGMT_SERVICE_PORT=8889
export HOST=127.0.0.1  # HOSTNAME MUST BE SETUP PROPERLY OR SET THIS VALUE
export HADOOP_HOSTNAME=localhost
export HADOOP_HOST_PORT=8020


##
## JUST LAUNCHER ARGS - only needed for the init.d script.
##

## launcher args
export CONFIG_APPLICATION=configserver
export CONFIG_APPLICATION_EXTENSION=jar
export PATH_TO_EXECUTABLE=/home/ubuntu/${CONFIG_APPLICATION} # <-- no trailing slash here.

# adding this here b/c we'll just use the directory the executable is in
export DIRECTORY_FOR_GIT_REPO=${PATH_TO_EXECUTABLE}/gitConfiguration

# jvm arguments added here
export JVM_OPTS="-server -Xmx1536M -Xms1536M -XX:+UseConcMarkSweepGC -XX:+AggressiveOpts "
export SPRING_OPTS="--spring.profiles.active=production"
